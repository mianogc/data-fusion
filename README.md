# Introduzione

Questo repository contiene il codice sorgente relativo alla tesina per l'esame di *Sistemi Informativi* 
(tenuto dal professor Domenico Beneventano), del corso di laurea Magistrale di Ingegneria Informatica.

Questi sono i contenuti concordati con il docente:

* Tema principale: **Data Fusion**
* **Data Integration** trattada ad alto livello
* Utilizzo del materiale del professor Bizer per lo sviluppo del progetto e relativa tesina
* Check dei risultati ottenuti sul dataset Luna dong relativo alle azioni di borsa
* Utilizzo del framework Winter per l'implementazione

# Risorse

* Materiale del corso del professor Bizer ([qui](https://www.uni-mannheim.de/dws/teaching/course-details/courses-for-master-candidates/course-archive/hws-2018/ie-670-web-data-integration-hws2018/#c140150))
* Approfondimento su densità attributi e consistenza ([qui](http://ceur-ws.org/Vol-2400/paper-17.pdf))
* Dataset Luna Dong ([qui](http://lunadong.com/fusionDataSets.htm))